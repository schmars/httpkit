# The HTTP toolkit for Ruby 

HTTPkit is a Ruby toolkit for building HTTP clients and servers,
as well as compositions of them.

[![Build Status](https://travis-ci.org/lgierth/httpkit.png?branch=master)](https://travis-ci.org/lgierth/httpkit) [![Code Climate](https://codeclimate.com/github/lgierth/httpkit.png)](https://codeclimate.com/github/lgierth/httpkit) [![Coverage Status](https://coveralls.io/repos/lgierth/httpkit/badge.png?branch=master)](https://coveralls.io/r/lgierth/httpkit?branch=master)

- \#1 feature: readable, high-quality, extendable code with 89.01% mutation coverage (wip)
- \#2 feature: sophisticated request and response streaming
- \#3 feature: compatible with Rack, Faraday, Webmachine for Ruby, and VCR (all todo)
- \#4 feature: backed by Celluloid (wip) or Eventmachine

*Note:* The `master` branch contains the in-progress rewrite towards
HTTPkit 1.0. Look at the `0.5.x` branch for stable, but outdated and largely
unmaintained releases. HTTPkit used to be called Hatetepe.

## Installation

Add this line to your application's Gemfile:

    gem 'httpkit', '0.6.0.pre.3'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install httpkit --pre

## Usage

Code examples

- [Getting started ](http://rubydoc.info/github/lgierth/httpkit/master/file/examples/getting_started.rb)
- [HTTP echo server](http://rubydoc.info/github/lgierth/httpkit/master/file/examples/echo_server.rb)

API Reference

- [HTTPkit::Client    ](http://rubydoc.info/github/lgierth/httpkit/master/HTTPkit/Client)
- [HTTPkit::Server    ](http://rubydoc.info/github/lgierth/httpkit/master/HTTPkit/Server)
- [HTTPkit::Response  ](http://rubydoc.info/github/lgierth/httpkit/master/HTTPkit/Request)
- [HTTPkit::Request   ](http://rubydoc.info/github/lgierth/httpkit/master/HTTPkit/Response)
- [HTTPkit::Body      ](http://rubydoc.info/github/lgierth/httpkit/master/HTTPkit/Body)
- [HTTPkit::Promise   ](http://rubydoc.info/github/lgierth/httpkit/master/HTTPkit/Promise)
- [HTTPkit::Serializer](http://rubydoc.info/github/lgierth/httpkit/master/HTTPkit/Serializer)

## To do

Here: https://trello.com/b/OoxEq1ze/httpkit

## License

HTTPkit is free and unencumbered public domain software. For more
information, see [unlicense.org](http://unlicense.org/) or the accompanying
UNLICENSE file.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
