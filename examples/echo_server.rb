# encoding: utf-8

require 'httpkit'

class EchoServer
  def serve(request, served)
    response = streaming_response
    served.fulfill(response)

    serialize(request, response)
    request.closed { response.close }
  end

  private

  def streaming_response
    HTTPkit::Response.new(200, { 'Content-Type' => 'text/plain' },
                          HTTPkit::Body.new)
  end

  def serialize(request, response)
    writer     = response.body.closed.method(:progress)
    serializer = HTTPkit::Serializer.new(request, writer)
    serializer.serialize
  end
end

HTTPkit.start do
  HTTPkit::Server.start(address:  ENV.fetch('ADDRESS', '127.0.0.1'),
                        port:     ENV.fetch('PORT', 3000).to_i,
                        handlers: [HTTPkit::Server::KeepAliveHandler.new,
                                   EchoServer.new])

  Signal.trap(:INT)  { HTTPkit.stop }
  Signal.trap(:TERM) { HTTPkit.stop }
end
