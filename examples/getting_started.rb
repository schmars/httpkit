# encoding: utf-8

require 'httpkit'

class HelloServer
  def serve(request, served)
    p request.http_method # => :get
    p request.uri         # => "/"
    p request.headers     # => {"Host"=>"127.0.0.1:3000",
                          #     "Content-Length"=>"0"}
    p request.body.to_s   # => ""

    served.fulfill(response)
  end

  def response
    HTTPkit::Response.new(200, { 'Content-Type' => 'text/plain' }, 'hello')
  end
end

HTTPkit.run do
  HTTPkit::Server.start(address:  '127.0.0.1', port: 3000,
                        handlers: [HelloServer.new])

  client = HTTPkit::Client.start(address: '127.0.0.1', port: 3000)

  response = client.request(:get, '/')

  p response.status      # => 200
  p response.status_name # => "OK"
  p response.headers     # => {"Content-Type"=>"text/plain",
                         #     "Content-Length"=>"5"}
  p response.body.to_s   # => "hello"
end
