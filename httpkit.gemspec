# encoding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'httpkit/version'

Gem::Specification.new do |spec|
  spec.name          = "httpkit"
  spec.version       = HTTPkit::VERSION
  spec.authors       = ["Lars Gierth"]
  spec.email         = ["lars.gierth@gmail.com"]
  spec.summary       = %q{The HTTP toolkit for Ruby}
  spec.description   = %q{HTTPkit is a Ruby toolkit for building HTTP clients and servers, as well as compositions of them.}
  spec.homepage      = "https://github.com/lgierth/httpkit"
  spec.license       = "Public Domain"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^spec/})
  spec.require_paths = ["lib"]

  spec.add_dependency "eventmachine"
  spec.add_dependency "http_parser.rb"
  spec.add_dependency "promise.rb", "~> 0.6"

  spec.add_development_dependency "rspec"
end
