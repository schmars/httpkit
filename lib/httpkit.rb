# encoding: utf-8

require 'eventmachine'
require 'fiber'
require 'promise'
require 'http/parser'
require 'time'

require 'httpkit/version'

require 'httpkit/support/handler_manager'
require 'httpkit/support/message'

require 'httpkit/promise'
require 'httpkit/body'
require 'httpkit/request'
require 'httpkit/response'

require 'httpkit/connection/eventmachine'
require 'httpkit/connection/status'
require 'httpkit/serializer'

require 'httpkit/client'
require 'httpkit/client/mandatory_handler'
require 'httpkit/client/keep_alive_handler'
require 'httpkit/client/timeouts_handler'
require 'httpkit/server'
require 'httpkit/server/mandatory_handler'
require 'httpkit/server/keep_alive_handler'
require 'httpkit/server/timeouts_handler'

module HTTPkit
  def self.run
    start do
      yield
      stop
    end
  end

  def self.start
    EM.run do
      Fiber.new { yield }.resume
    end
  end

  def self.stop
    EM.stop
    EM.next_tick {}
  end

  def self.sleep(duration)
    promise = Promise.new
    EM.add_timer(duration) { promise.fulfill }
    promise.sync
  end
end
