# encoding: utf-8

module HTTPkit
  class Client
    include Support::HandlerManager::Setup
    include Connection::Status

    def self.start(config)
      Connection::EventMachine.start_client(config, self)
    end

    def self.request(*args)
      uri = URI(args[1])
      client = start(address: uri.host, port: uri.port)
      client.request(*args)
    end

    attr_reader :config

    def initialize(config, connection)
      @config = config
      (@config[:handlers] ||= []) << MandatoryHandler.new
      @requests = []

      setup_connection(connection)
      setup_handlers
    end

    def request(*args)
      request = Request.new(*args)
      perform(request).sync
    end

    def perform(request)
      served = Promise.new

      if closed?
        served.reject(@connection.closed.reason)
      else
        # XXX: this index should be based on the request's sequence number,
        #      similar to Server::KeepAliveHandler
        @requests << [request, served]
        perform!(request)
      end

      served
    end

    def receive(response)
      request, served = find_request

      if request
        # XXX: fulfillment should happen after invoke(:receive), so that all
        #      handlers have run when the application code resumes
        served.fulfill(response)
        request, response = @handlers.invoke(:receive, request, response)
        response.closed { finish(request) }
      end
    end

    def finish(request)
      @requests.delete_if { |(req)| req == request }
      @handlers.invoke(:finish, request)
    end

    def teardown(reason)
      @requests.each do |_, served|
        served.reject(reason)
        if (response = served.value)
          response.reject_closed(reason)
        end
      end
    end

    private

    def perform!(request)
      Fiber.new do
        @handlers.invoke(:perform, request)
        @connection.serialize(request)
      end.resume
    end

    def find_request
      @requests.detect { |_, served| served.pending? }
    end

    def setup_connection(connection)
      @connection = connection
      @connection.on_message = method(:receive)
      @connection.closed.then(method(:teardown), method(:teardown))
    end
  end
end
