# encoding: utf-8

module HTTPkit
  class Client::KeepAliveHandler
    def perform(request)
      @previous, previous = request, @previous
      previous.closed! if previous
    end
  end
end
