# encoding: utf-8

module HTTPkit
  class Client::MandatoryHandler
    USER_AGENT = 'User-Agent'.freeze
    USER_AGENT_VALUE = "httpkit/#{HTTPkit::VERSION}".freeze
    HOST = 'Host'.freeze
    HOST_VALUE = '%s:%d'.freeze

    def setup(config, _, _)
      @config = config
    end

    def perform(request)
      request.headers.merge!(missing_headers(request))
      yield request
    end

    private

    def missing_headers(request)
      headers.reject { |k, _| request.headers.key?(k) }
    end

    def headers
      host = sprintf(HOST_VALUE, @config[:address], @config[:port])
      { USER_AGENT => USER_AGENT_VALUE, HOST => host }
    end
  end
end
