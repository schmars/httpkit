# encoding: utf-8

module HTTPkit
  # @see EM.heartbeat_interval
  class Client::TimeoutsHandler
    def setup(config, _, connection)
      @config = config
      @connection = connection

      @connection.comm_inactivity_timeout = @config[:timeout]         ||= 2.0
      @connection.pending_connect_timeout = @config[:connect_timeout] ||= 2.0
    end
  end
end
