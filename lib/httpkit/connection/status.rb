# encoding: utf-8

module HTTPkit
  module Connection
    module Status
      def close(reason = nil)
        @connection.close(reason)
      end

      def closed?
        !@connection.closed.pending?
      end

      def error?
        @connection.closed.rejected?
      end

      def network_fault?
        [Errno::ENOTCONN, Errno::ENETUNREACH]
          .include?(@connection.closed.reason)
      end

      def timeout?
        @connection.closed.reason == Errno::ETIMEDOUT
      end
    end
  end
end
