# encoding: utf-8

module HTTPkit
  class Promise < ::Promise
    def wait
      fiber  = Fiber.current
      resume = proc { fiber.resume }
      self.then(resume, resume)

      Fiber.yield
    end

    private

    def defer
      EM.next_tick { yield } if EM.reactor_running?
    end
  end
end
