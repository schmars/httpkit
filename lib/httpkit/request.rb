# encoding: utf-8

module HTTPkit
  Request = Struct.new(:http_method, :uri, :headers, :body, :http_version)

  class Request
    private :http_method=, :uri=, :headers=, :body=

    include Support::Message

    # TODO: URI.join is really slow
    def initialize(http_method, uri, headers = {}, body = '')
      super(http_method,
            # URI('http://' + uri)
            URI.join('http:///', uri).request_uri,
            headers, Body.build(body), 1.1)
    end
  end
end
