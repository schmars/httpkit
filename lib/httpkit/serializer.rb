# encoding: utf-8

module HTTPkit
  # Streaming HTTP message serializer.
  #
  # @api public
  #
  class Serializer
    REQUEST_LINE = "%s %s HTTP/%.1f\r\n".freeze
    RESPONSE_LINE = "HTTP/%.1f %d %s\r\n".freeze
    HEADER = "%s: %s\r\n".freeze
    TRANSFER_ENCODING = 'Transfer-Encoding'.freeze
    CHUNKED = 'chunked'.freeze
    CONTENT_LENGTH = 'Content-Length'.freeze
    CRLF = "\r\n".freeze

    def initialize(message, writer)
      @message, @writer = message, writer
      @streaming = message.body.closed.pending?
    end

    def serialize
      write(first_line)
      write(header_block)
      write_body
    end

    private

    def write(*data)
      @writer.call(data.join)
    end

    def first_line
      if Request === @message
        request_line
      else
        response_line
      end
    end

    def request_line
      sprintf(REQUEST_LINE,
              @message.http_method.upcase, @message.uri, @message.http_version)
    end

    def response_line
      sprintf(RESPONSE_LINE,
              @message.http_version, @message.status, @message.status_name)
    end

    def header_block
      @message.headers
        .merge(encoding_headers)
        .reduce('') { |a, e| a << header_line(*e) } + CRLF
    end

    def encoding_headers
      if streaming?
        { TRANSFER_ENCODING => CHUNKED,
          CONTENT_LENGTH => nil }
      else
        { TRANSFER_ENCODING => nil,
          CONTENT_LENGTH => @message.body.to_s.bytesize }
      end
    end

    def streaming?
      @streaming
    end

    def header_line(key, value)
      if value.to_s.empty?
        ''
      else
        sprintf(HEADER, key, value)
      end
    end

    def write_body
      if streaming?
        @message.body.each { |chunk| write_chunk(chunk) }
        write_chunk!('')
      else
        write(@message.body)
      end
    end

    def write_chunk(chunk)
      write_chunk!(chunk) unless chunk.empty?
    end

    def write_chunk!(chunk)
      write(chunk.bytesize.to_s(16), CRLF, chunk, CRLF)
    end
  end
end
