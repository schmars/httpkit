# encoding: utf-8

module HTTPkit
  class Server::KeepAliveHandler
    CONNECTION = 'Connection'.freeze
    CLOSE = 'close'.freeze
    KEEP_ALIVE = 'keep-alive'.freeze

    def setup(_, server, _)
      @server = server
      @requests, @previous = {}, nil
    end

    # XXX: we don't need @previous and @requests if we store the request's
    #      sequence number per connection within the request. that way, the
    #      request object is merely a representation of the actual request.
    def serve(request, served)
      @requests[request] = [served, @previous]
      @previous = request
    end

    def respond(request, response)
      synchronize_responses(request)

      if close_connection?(request)
        yield request, close_response(response)
      else
        yield request, keep_alive_response(response)
      end
    end

    def finish(request)
      @requests.delete(request)
    end

    private

    def close_response(response)
      response.headers[CONNECTION] = CLOSE
      # XXX: possible race condition with other locations waiting for this
      response.closed { @server.close }
      response
    end

    def keep_alive_response(response)
      response.headers[CONNECTION] = KEEP_ALIVE
      response
    end

    def synchronize_responses(request)
      _, previous = @requests[request]
      served, _   = @requests[previous]

      served.sync.closed! if served
    end

    def connection_header(request)
      response = @requests[request][0].value
      request.headers[CONNECTION] || response.headers[CONNECTION]
    end

    def close_connection?(request)
      header = connection_header(request).to_s.downcase
      header == CLOSE || http10_close?(request.http_version, header)
    end

    def http10_close?(version, header)
      version < 1.1 && header != KEEP_ALIVE
    end
  end
end
