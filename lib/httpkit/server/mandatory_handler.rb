# encoding: utf-8

module HTTPkit
  class Server::MandatoryHandler
    SERVER = 'Server'.freeze
    SERVER_VALUE = "httpkit/#{HTTPkit::VERSION}".freeze
    DATE = 'Date'.freeze

    def respond(_request, response)
      response.headers.merge!(missing_headers(response))
      yield _request, response
    end

    private

    def missing_headers(response)
      headers.reject { |k, _| response.headers.key?(k) }
    end

    def headers
      { SERVER => SERVER_VALUE, DATE => Time.now.httpdate }
    end
  end
end
