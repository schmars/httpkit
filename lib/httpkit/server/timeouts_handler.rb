# encoding: utf-8

module HTTPkit
  # @see EM.heartbeat_interval
  class Server::TimeoutsHandler
    def setup(config, _, connection)
      @config = config
      @connection = connection

      @connection.comm_inactivity_timeout = @config[:timeout] ||= 2.0
    end
  end
end
