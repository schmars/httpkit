# encoding: utf-8

module HTTPkit
  module Support
    class HandlerManager
      def initialize(handlers)
        @handlers = handlers
      end

      def invoke(message, *arguments)
        @handlers
          .select { |handler| handler.respond_to?(message) }
          .reduce(arguments) do |args, handler|
            handler.send(message, *args) { |*new_args| args = new_args }
            args
          end
      end

      # XXX not mutation covered
      module Setup
        def setup_handlers
          @handlers = Support::HandlerManager.new(@config[:handlers] || [])
          @handlers.invoke(:setup, @config, self, @connection)
        end
      end
    end
  end
end
