# encoding: utf-8

module HTTPkit
  module Support
    module Message
      def closed?
        !body.closed.pending?
      end

      def closed!
        body.closed.sync
      end

      def closed(&block)
        body.closed.then(&block)
      end

      def close
        body.closed.fulfill
      end

      def reject_closed(reason)
        body.closed.reject(reason)
      end

      def self.build(parser)
        if parser.http_method
          build_request(parser)
        else
          build_response(parser)
        end
      end

      private

      def self.build_request(parser)
        request = Request.new(http_method_from(parser),
                              parser.request_url,
                              parser.headers,
                              Body.new)
        request.http_version = http_version_from(parser)
        request
      end

      def self.build_response(parser)
        response = Response.new(parser.status_code,
                                parser.headers,
                                Body.new)
        response.http_version = http_version_from(parser)
        response
      end

      def self.http_method_from(parser)
        parser.http_method.downcase.to_sym
      end

      def self.http_version_from(parser)
        parser.http_version.join('.').to_f
      end
    end
  end
end
