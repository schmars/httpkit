# encoding: utf-8

require 'spec_helper'

describe 'Client error handling', reactor: true do
  include_context :server_client_pair

  let(:served) { client.perform(open_request) }

  subject! { client.close(reason) }

  describe 'on connection error' do
    let(:reason) { Errno::ENETUNREACH }

    it 'rejects promises' do
      expect(client).to be_closed
      expect(client).to be_error
      expect(client).to be_network_fault

      expect(served).to be_rejected
      expect(served.reason).to be(reason)
    end
  end

  describe 'on timeout' do
    let(:reason) { Errno::ETIMEDOUT }

    it 'rejects promises' do
      expect(client).to be_closed
      expect(client).to be_error
      expect(client).to be_timeout

      expect(served).to be_rejected
      expect(served.reason).to be(reason)
    end
  end
end

describe 'Server error handling', reactor: true do
  include_context :server_client_pair

  describe 'on application error' do
    let(:server_config) { { handlers: [handler] } }
    let(:handler) { double('handler') }
    let(:reason) { RuntimeError.new }

    before do
      allow(handler).to receive(:serve) { raise reason }
    end

    subject! do
      client.perform(open_request)
      tick(2)
    end

    it 'rejects promises' do
      expect(server).to be_closed
      expect(server).to be_error
    end
  end

  describe 'on timeout' do
    subject! do
      client.perform(open_request)
      tick(1)
      server.close(Errno::ETIMEDOUT)
      tick(1)
    end

    it 'rejects promises' do
      expect(server).to be_closed
      expect(server).to be_error
      expect(server).to be_timeout
    end
  end
end
