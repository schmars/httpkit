# encoding: utf-8

require 'spec_helper'

describe reactor: true do
  include_context :server_client_pair

  let(:server_config) do
    { handlers: [HTTPkit::Server::KeepAliveHandler.new, SpecHandler.new] }
  end
  let(:client_config) do
    { handlers: [HTTPkit::Client::KeepAliveHandler.new] }
  end

  let!(:served) do
    requests.map { |request| client.perform(request) }
  end
  let(:responses) { served.map(&:sync) }

  describe HTTPkit::Server, 'with KeepAliveHandler' do
    let(:requests) do
      [closed_request(:get, '/sleep'), closed_request]
    end

    # XXX: tends to fail
    it 'sends responses in the correct order' do
      expect(responses[0].body.to_s).to eq('/sleep')
      expect(responses[1].body.to_s).to eq('/')
    end

    describe 'and HTTP/1.1 request' do
      let(:requests) { [closed_request] }

      it 'assumes Connection: keep-alive' do
        expect(client).not_to be_closed
        expect(server).not_to be_closed

        expect(responses[0].headers).to include('Connection' => 'keep-alive')
      end
    end

    describe 'and HTTP/1.0 request' do
      let(:requests) do
        [closed_request.tap { |r| r.http_version = 1.0 }]
      end

      it 'assumes Connection: close' do
        responses
        tick(2)
        expect(client).to be_closed
        expect(server).to be_closed

        expect(responses[0].headers).to include('Connection' => 'close')
      end
    end

    describe 'and Connection: keep-alive' do
      let(:requests) do
        [closed_request(:get, '/', 'Connection' => 'keep-alive')
          .tap { |r| r.http_version = 1.0 }]
      end

      it 'keeps the connection open' do
        expect(client).not_to be_closed
        expect(server).not_to be_closed

        expect(responses[0].headers).to include('Connection' => 'keep-alive')
      end
    end

    describe 'and Connection: close' do
      let(:requests) do
        [closed_request(:get, '/', 'Connection' => 'close')]
      end

      it 'closes the connection after sending the response' do
        responses
        tick(2)
        expect(client).to be_closed
        expect(server).to be_closed

        expect(responses[0].headers).to include('Connection' => 'close')
      end
    end
  end

  describe HTTPkit::Client, 'with KeepAliveHandler' do
    let(:requests) do
      [open_request(:get, '/?1'), closed_request(:get, '/?2')]
    end

    before do
      requests[0].close
    end

    it 'sends one request at a time' do
      expect(responses[0].body.to_s).to eq('/?1')
      expect(responses[1].body.to_s).to eq('/?2')
    end
  end
end
