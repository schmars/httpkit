# encoding: utf-8

require 'spec_helper'

describe 'Smoke test', reactor: true do
  include_context :server_client_pair

  let(:response) { client.request(:get, '/') }
  let(:request)  { intercepted_requests[0] }

  it 'exchanges request and response' do
    expect(response.status).to  be(200)
    expect(response.headers).to include('Content-Length' => '1')
    expect(response.body.to_s).to eq('/')

    expect(request.http_method).to be(:get)
    expect(request.uri).to         eq('/')
    expect(request.headers).to     include('Content-Length' => '0')
    expect(request.body.to_s).to   eq('')
  end
end
