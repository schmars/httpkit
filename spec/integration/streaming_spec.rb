# encoding: utf-8

require 'spec_helper'

describe 'Request body streaming', reactor: true do
  include_context :server_client_pair

  let(:chunks) { [] }

  before do
    client.perform(request)
    tick(2)
    Fiber.new do
      intercepted_requests[0].body.each do |chunk|
        chunks << chunk
      end
    end.resume
  end

  let(:request) { open_request }

  subject! do
    %w[foo bar baz].each do |chunk|
      request.body.closed.progress(chunk)
      tick
    end
    request.close
    tick
  end

  it 'progressively sends the body' do
    expect(intercepted_requests[0].headers)
      .to include('Transfer-Encoding' => 'chunked')

    expect(chunks).to eq(%w[foo bar baz])
  end
end

describe 'Response body streaming', reactor: true do
  include_context :server_client_pair

  let(:chunks) { [] }

  let(:response) { client.request(:get, '/streaming') }

  subject! { response }

  before do
    response.body.each do |chunk|
      chunks << chunk
    end
  end

  it 'progressively receives the body' do
    expect(chunks).to eq(%w[foo bar baz])
  end
end
