# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Client, 'in connecting state', reactor: true do
  # connect to a non-existing host to provoke connection timeout
  let(:address) { '1.2.3.4' }
  let(:client_config) do
    { address: address, port: random_port,
      handlers: [HTTPkit::Client::TimeoutsHandler.new] }
  end
  let!(:client)  { HTTPkit::Client.start(client_config) }

  after { client.close }

  it 'times out after 2 seconds' do
    # only test the config, and don't actually wait for 2 seconds
    expect(client.config).to include(connect_timeout: 2.0)
  end

  describe 'with :connect_timeout option' do
    let(:client_config) do
      { address: address, port: random_port, connect_timeout: 0.01,
        handlers: [HTTPkit::Client::TimeoutsHandler.new] }
    end

    it 'times out after $n seconds' do
      async_sleep(0.015)
      if client.network_fault?
        pending 'Requires network connection'
      else
        expect(client).to be_timeout
      end
    end
  end
end

describe HTTPkit::Client, 'in idle state', reactor: true do
  include_context :server_client_pair

  let(:client_config) { { handlers: [HTTPkit::Client::TimeoutsHandler.new] } }

  it 'times out after 2 seconds' do
    # only test the config, and don't actually wait for 2 seconds
    expect(client.config).to include(connect_timeout: 2.0)
  end

  describe 'with :timeout option' do
    let(:client_config) do
      { timeout: 0.01, handlers: [HTTPkit::Client::TimeoutsHandler.new] }
    end

    it 'times out after $n seconds' do
      async_sleep(0.015)
      expect(client).to be_timeout
      expect(server).to be_closed
    end
  end
end

describe HTTPkit::Server, 'in idle state', reactor: true do
  include_context :server_client_pair

  let(:server_config) { { handlers: [HTTPkit::Server::TimeoutsHandler.new] } }

  it 'times out after 2 seconds' do
    # only test the config, and don't actually wait for 2 seconds
    expect(server.config).to include(timeout: 2.0)
  end

  describe 'with :timeout option' do
    let(:server_config) do
      { timeout: 0.01, handlers: [HTTPkit::Server::TimeoutsHandler.new] }
    end

    it 'times out after $n seconds' do
      async_sleep(0.015)
      expect(client).to be_closed
      expect(server).to be_timeout
    end
  end
end
