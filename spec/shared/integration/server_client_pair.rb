# encoding: utf-8

shared_context :server_client_pair do
  let(:server_config) { { handlers: [SpecHandler.new] } }
  let(:client_config) { { handlers: [] } }
  let(:interceptor)   { double('interceptor') }

  let(:server_and_client) do
    server_client_pair(server_config, client_config, interceptor)
  end

  let(:intercepted_requests) { [] }
  let(:intercepted_promises) { [] }

  before do
    allow(interceptor).to receive(:serve) { |request, served|
      intercepted_requests << request
      intercepted_promises << served
    }
  end

  let!(:server) { server_and_client[0] }
  let!(:client) { server_and_client[1] }

  after do
    server.close
    client.close
  end
end
