# encoding: utf-8

if ENV['COVERAGE'] == 'true'
  require 'simplecov'
  require 'coveralls'

  SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[
    SimpleCov::Formatter::HTMLFormatter,
    Coveralls::SimpleCov::Formatter
  ]

  SimpleCov.start do
    command_name 'spec:unit'

    add_filter 'config'
    add_filter 'spec'
  end
end

require 'httpkit'
require 'weakref'

require 'awesome_print'

require 'devtools/spec_helper'

RSpec.configure do |config|
  config.include(SpecHelper)

  config.around do |example|
    if example.metadata[:reactor]
      EM.run do
        EM.add_timer(0.5) { raise 'Example timed out' }

        Fiber.new do
          example.call
          EM.stop
          EM.next_tick {}
        end.resume
      end
    else
      Timeout.timeout(0.5) { example.call }
    end
  end
end
