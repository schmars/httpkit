# encoding: utf-8

class SpecHandler
  def serve(request, served)
    served.fulfill(response_for(request))
  end

  private

  def response_for(request)
    case request.uri
    when '/sleep'     then sleep_response
    when '/close'     then close_response
    when '/streaming' then streaming_response
    else                   echo_response(request)
    end
  end

  def echo_response(request)
    HTTPkit::Response.new(200, { 'Content-Type' => 'text/plain' },
                          request.uri)
  end

  def sleep_response
    SpecHelper.async_sleep(0.01)
    HTTPkit::Response.new(200, { 'Content-Type' => 'text/plain' }, '/sleep')
  end

  def close_response
    HTTPkit::Response.new(200,
                          { 'Content-Type' => 'text/plain',
                            'Connection' => 'close' },
                          '/close')
  end

  def streaming_response
    response = HTTPkit::Response.new(200, { 'Content-Type' => 'text/plain' },
                                     HTTPkit::Body.new)
    SpecHelper.defer do
      %w[foo bar baz].each do |chunk|
        SpecHelper.tick(2)
        response.body.closed.progress(chunk)
      end
      response.close
    end
    response
  end
end
