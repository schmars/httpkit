# encoding: utf-8

module SpecHelper
  def async_sleep(seconds)
    fiber = Fiber.current
    EM.add_timer(seconds) { fiber.resume }
    Fiber.yield
  end
  module_function :async_sleep

  def tick(ticks = 1)
    fiber = Fiber.current
    EM.next_tick { fiber.resume }
    Fiber.yield

    tick(ticks - 1) if ticks > 1
  end
  module_function :tick

  def defer
    EM.next_tick do
      Fiber.new { yield }.resume
    end
  end
  module_function :defer

  def open_request(http_method = :get, uri = '/', headers = {})
    HTTPkit::Request.new(http_method, uri, headers, HTTPkit::Body.new)
  end

  def closed_request(*args)
    open_request(*args).tap(&:close)
  end

  def open_response(status = 200, headers = {})
    HTTPkit::Response.new(status, headers, HTTPkit::Body.new)
  end

  def closed_response(*args)
    open_response(*args).tap(&:close)
  end

  def localhost
    '127.0.0.1'
  end

  def random_port
    server = TCPServer.new(localhost, 0)
    server.addr[1]
  ensure
    server.shutdown
  end

  def server_client_pair(server_config, client_config, interceptor)
    server = nil
    inspect_server(server_config, interceptor) { |_, s, _| server = s }

    config = { address: localhost, port: random_port }
    HTTPkit::Server.start(config.merge(server_config))
    client = HTTPkit::Client.start(config.merge(client_config))

    tick
    [server, client]
  end

  def inspect_server(server_config, interceptor, &block)
    allow(interceptor).to receive(:setup, &block)
    server_config[:handlers].unshift(interceptor)
  end
end
