# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Client::MandatoryHandler do
  describe '#perform' do
    let(:handler) { described_class.new }
    let(:request) { double('request', headers: headers) }
    let(:headers) { {} }

    subject! do
      handler.setup({ address: 'example.net', port: 80 }, nil, nil)
      handler.perform(request) { |req| @request = req }
    end

    it 'sets User-Agent and Host headers' do
      expect(@request.headers['User-Agent'])
        .to eq("httpkit/#{HTTPkit::VERSION}")
      expect(@request.headers['Host']).to eq('example.net:80')
    end

    describe 'with User-Agent or Host header set to anything' do
      let(:headers) { { 'User-Agent' => nil, 'Host' => nil } }

      it 'does not override' do
        expect(@request.headers['User-Agent']).to be(nil)
        expect(@request.headers['Host']).to be(nil)
      end
    end
  end
end
