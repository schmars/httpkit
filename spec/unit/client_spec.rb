# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Client do
  let(:client) { described_class.new(config, connection) }

  let(:config) do
    { address: localhost, port: random_port, handlers: [handler] }
  end
  let(:connection) do
    double('connection', :'on_message=' => nil,
                         closed: double('closed', then: nil,
                                                  fulfill: nil,
                                                  pending?: true,
                                                  reason: nil),
                         serialize: nil,
                         close: nil)
  end
  let(:handler) do
    double('handler', setup: nil, receive: nil, finish: nil)
  end

  describe '.start' do
    subject { described_class.start(config) }

    it 'starts an EventMachine-backed client instance' do
      expect(HTTPkit::Connection::EventMachine)
        .to receive(:start_client).with(config, described_class) { client }
      expect(subject).to be(client)
    end
  end

  describe '.request' do
    let(:arg) { double }
    let(:uri) { 'http://localhost:3000' }

    let(:options)  { { address: 'localhost', port: 3000 } }
    let(:client)   { double }
    let(:response) { double }

    before do
      allow(described_class).to receive(:start).with(options) { client }
      allow(client).to receive(:request).with(arg, uri, arg, arg) { response }
    end

    subject { described_class.request(arg, uri, arg, arg) }

    it 'starts a client and performs the request' do
      expect(subject).to be(response)
    end
  end

  describe '#initialize' do
    let(:on_message) { client.method(:receive) }
    let(:teadown) { client.method(:teardown) }

    subject! { client }

    its(:config) { should be(config) }

    it 'wires up #receive and #teardown' do
      expect(connection).to have_received(:on_message=).with(on_message)
      expect(connection.closed).to have_received(:then).with(teadown, teadown)
    end

    it 'sets up handlers' do
      expect(handler).to have_received(:setup).with(config, client, connection)
      expect(config[:handlers].last)
        .to be_a(HTTPkit::Client::MandatoryHandler)
    end

    describe 'without handlers' do
      let(:config) { {} }

      it 'adds the mandatory handler' do
        expect(config[:handlers].last)
          .to be_a(HTTPkit::Client::MandatoryHandler)
      end
    end
  end

  describe '#request' do
    let(:request) do
      double('request', body: double(closed: double('closed', fulfill: nil)))
    end
    let(:response) { double('response') }
    let(:served)   { double('served', sync: response) }

    before do
      allow(HTTPkit::Request).to receive(:new)     { request }
      allow(client).to            receive(:perform) { served }
    end

    subject! { client.request(:head, '/wat') }

    specify do
      expect(HTTPkit::Request).to have_received(:new).with(:head, '/wat')
      expect(client).to            have_received(:perform).with(request)

      expect(subject).to be(response)
    end
  end

  describe '#perform' do
    let(:request)       { double('request', headers: {}) }
    let(:closed_reason) { nil }

    before do
      allow(connection.closed).to receive(:pending?) { closed_reason.nil? }
      allow(connection.closed).to receive(:reason)   { closed_reason }

      @fibers = []
      allow(handler).to    receive(:perform) { @fibers << Fiber.current }
      allow(connection).to receive(:serialize) { @fibers << Fiber.current }
    end

    subject! { client.perform(request) }

    it 'sends the request on the wire' do
      expect(handler).to    have_received(:perform).with(request)
      expect(connection).to have_received(:serialize).with(request)

      expect(@fibers).not_to   include(Fiber.current)
      expect(@fibers.uniq).to  be_one
    end

    describe 'with closed connection' do
      let(:closed_reason) { double }

      it 'rejects the request' do
        expect(subject).to        be_rejected
        expect(subject.reason).to be(closed_reason)
        expect(connection).to_not have_received(:serialize)
      end
    end
  end

  describe '#receive' do
    let(:request)        { open_request }
    let(:other_request)  { open_request }
    let(:response)       { open_response }
    let(:other_response) { open_response }

    describe 'with outstanding request' do
      let!(:served)       { client.perform(request) }
      let!(:other_served) { client.perform(other_request) }

      subject! do
        client.receive(response)
        client.receive(other_response)
      end

      it 'correlates response with request, and notifies handlers' do
        expect(served).to       be_fulfilled
        expect(served.value).to be(response)
        expect(other_served).to       be_fulfilled
        expect(other_served.value).to be(other_response)

        expect(handler).to have_received(:receive).with(request, response)
        expect(handler)
          .to have_received(:receive).with(other_request, other_response)
      end
    end

    describe 'without outstanding request' do
      subject! { client.receive(response) }

      it 'does not do anything' do
        expect(handler).not_to have_received(:receive)
      end
    end

    describe 'wiring', reactor: true do
      let(:request)  { closed_request }
      let(:response) { open_response }

      before do
        client.perform(request)

        allow(client).to receive(:finish)
      end

      subject! do
        client.receive(response)
        response.close
        tick
      end

      it 'wires up #finish' do
        client.should have_received(:finish).with(request)
      end
    end

    [:fulfill, :reject].each do |action|
      describe "after #{action}ed response" do
        let(:request)  { WeakRef.new(closed_request) }
        let(:response) { WeakRef.new(open_response) }

        before do
          client.perform(request.__getobj__)
          client.receive(response.__getobj__)
        end

        subject do
          response.finished.send(action)
          tick
          RSpec::Mocks.teardown
          GC.start
        end

        xit 'cleans up' do
          expect { subject }.to change { request.weakref_alive? }.to(false)
        end
      end
    end
  end

  describe '#finish' do
    let(:request)        { open_request }
    let(:other_request)  { open_request }
    let(:response)       { open_response }

    before do
      client.perform(request)
      client.perform(other_request)
    end

    subject! do
      client.finish(request)
      client.receive(response)
    end

    it 'removes the request' do
      expect(handler).to have_received(:receive).with(other_request, response)
    end

    it 'notifies the handlers' do
      expect(handler).to have_received(:finish).with(request)
    end
  end

  describe '#teardown' do
    let(:requests)  { [closed_request, closed_request] }
    let(:responses) { [open_response] }
    let(:reason)    { double('reason') }

    let!(:served) do
      requests.map { |request| client.perform(request) }
    end

    before do
      client.receive(responses[0])
    end

    subject! { client.teardown(reason) }

    it 'rejects outstanding requests and responses' do
      expect(responses[0].body.closed).to        be_rejected
      expect(responses[0].body.closed.reason).to be(reason)
      expect(served[1]).to                    be_rejected
      expect(served[1].reason).to             be(reason)
    end
  end
end
