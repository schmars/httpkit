# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Connection::Status do
  let(:closed) { HTTPkit::Promise.new }
  let(:connection) { double('connection', close: nil, closed: closed) }

  let(:object) do
    Object.new.tap do |obj|
      obj.extend(described_class)
      obj.instance_variable_set(:@connection, connection)
    end
  end

  describe '#close with reason' do
    subject! { object.close }

    it 'forwards to @connection' do
      expect(connection).to have_received(:close).with(nil)
    end
  end

  describe '#close with reason' do
    let(:reason) { double }

    subject! { object.close(reason) }

    it 'forwards to @connection' do
      expect(connection).to have_received(:close).with(reason)
    end
  end

  describe '#closed?' do
    subject { object.closed? }
    it { should be(false) }

    describe 'if rejected' do
      before { closed.reject }
      it { should be(true) }
    end

    describe 'if fulfilled' do
      before { closed.reject }
      it { should be(true) }
    end
  end

  describe '#error?' do
    subject { object.error? }
    it { should be(false) }

    describe 'if rejected' do
      before { closed.reject }
      it { should be(true) }
    end
  end

  describe '#network_fault?' do
    subject { object.network_fault? }
    it { should be(false) }

    describe 'if rejected with ENETUNREACH' do
      before { closed.reject(Errno::ENETUNREACH) }
      it { should be(true) }
    end

    describe 'if rejected with ENOTCONN' do
      before { closed.reject(Errno::ENOTCONN) }
      it { should be(true) }
    end
  end

  describe '#timeout?' do
    subject { object.timeout? }
    it { should be(false) }

    describe 'if rejected with ENETUNREACH' do
      before { closed.reject(Errno::ETIMEDOUT) }
      it { should be(true) }
    end
  end
end
