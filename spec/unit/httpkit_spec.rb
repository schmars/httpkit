# encoding: utf-8

require 'spec_helper'

describe HTTPkit do
  describe '.run' do
    subject! do
      HTTPkit.run do
        @reactor = EM.reactor_running?
        @fiber   = Fiber.current
      end
    end

    it 'starts an EventMachine reactor' do
      expect(@reactor).to be(true)
    end

    it 'yields in a new Fiber' do
      expect(@fiber).not_to be(Fiber.current)
    end

    it 'stops the EventMachine reactor' do
      expect(EM.reactor_running?).to be(false)
    end
  end

  describe '.stop' do
    subject! do
      EM.run do
        EM.add_timer(1.0) { @timer = true }
        HTTPkit.stop
      end
    end

    it 'stops the EventMachine reactor' do
      expect(EM.reactor_running?).to be(false)
    end

    it 'stops EventMachine timers' do
      expect(@timer).not_to be(true)
    end
  end

  describe '.sleep', reactor: true do
    subject { HTTPkit.sleep(0.01) }

    let(:fiber) do
      @slept = false
      Fiber.new do
        subject
        @slept = true
      end
    end

    it 'sleeps for the given number of seconds' do
      fiber.resume
      expect(@slept).to be(false)
      sleep(0.01)
      tick(3)
      expect(@slept).to be(true)
    end
  end
end
