# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Promise do
  let(:promise) { described_class.new }

  describe '#defer' do
    before do
      allow(EM).to receive(:reactor_running?) { running }
      allow(EM).to receive(:next_tick)        { |&block| block.call }

      promise.then { @called = true }
    end

    subject! { promise.fulfill }

    describe 'with reactor' do
      let(:running) { true }

      it 'schedules the block for the next reactor iteration' do
        expect(@called).to be(true)
      end
    end

    describe 'without reactor' do
      let(:running) { false }

      it 'does nothing' do
        expect(@called).not_to be(true)
      end
    end
  end

  describe '#wait', reactor: true do
    let(:promise2) { described_class.new }

    before do
      promise.then { @fulfilled = true }
      promise2.then(nil, proc { @rejected = true })

      EM.next_tick { promise.fulfill }
      promise.then { promise2.reject }
    end

    subject! do
      promise.wait
      promise2.wait
    end

    it 'suspends the fiber until the promise state changes' do
      expect(@fulfilled).to be(true)
      expect(@rejected).to  be(true)
    end
  end
end
