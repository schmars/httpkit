# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Request do
  describe '#initialize' do
    subject do
      HTTPkit::Request.new(:get, '/a?b=c',
                           { 'Key' => 'value' }, 'hello').freeze
    end

    its(:http_method) { should be(:get) }
    its(:uri) { should eq('/a?b=c') }
    its(:headers) { should eq('Key' => 'value') }

    specify { expect(subject.body.read).to eq('hello') }

    describe 'defaults' do
      subject { HTTPkit::Request.new(:get, '/') }

      its(:headers) { should eq({}) }
      its(:http_version) { should eq(1.1) }

      specify { expect(subject.body.read).to be_empty }
    end
  end

  describe '#http_version=' do
    subject { HTTPkit::Request.new(:get, '/') }

    before { subject.http_version = 1.0 }

    its(:http_version) { should eq(1.0) }
  end
end
