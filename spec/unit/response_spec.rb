# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Response do
  describe '#initialize' do
    subject do
      HTTPkit::Response.new(200, { 'Key' => 'value' }, 'hello').freeze
    end

    its(:status) { should be(200) }
    its(:headers) { should eq('Key' => 'value') }

    specify { expect(subject.body.read).to eq('hello') }

    its(:status_name) { should eq('OK') }

    describe 'defaults' do
      subject { HTTPkit::Response.new(200) }

      its(:headers) { should eq({}) }
      its(:http_version) { should eq(1.1) }

      specify { expect(subject.body.read).to be_empty }
    end

    describe 'unknown status' do
      subject { HTTPkit::Response.new(-1) }

      its(:status) { should be(-1) }
      its(:status_name) { should eq('Unknown Status') }
    end
  end

  describe '#http_version=' do
    subject { HTTPkit::Response.new(200) }

    before { subject.http_version = 1.0 }

    its(:http_version) { should eq(1.0) }
  end

  describe 'with 1xx status' do
    subject { HTTPkit::Response.new(100) }

    its(:status_class) { should be(1) }

    it { should     be_informational }
    it { should_not be_successful }
    it { should_not be_redirection }
    it { should_not be_client_error }
    it { should_not be_server_error }
  end

  describe 'with 199 status' do
    subject { HTTPkit::Response.new(199) }

    its(:status_class) { should be(1) }
  end

  describe 'with 2xx status' do
    subject { HTTPkit::Response.new(200) }

    its(:status_class) { should be(2) }

    it { should_not be_informational }
    it { should     be_successful }
    it { should_not be_redirection }
    it { should_not be_client_error }
    it { should_not be_server_error }
  end

  describe 'with 3xx status' do
    subject { HTTPkit::Response.new(300) }

    its(:status_class) { should be(3) }

    it { should_not be_informational }
    it { should_not be_successful }
    it { should     be_redirection }
    it { should_not be_client_error }
    it { should_not be_server_error }
  end

  describe 'with 4xx status' do
    subject { HTTPkit::Response.new(400) }

    its(:status_class) { should be(4) }

    it { should_not be_informational }
    it { should_not be_successful }
    it { should_not be_redirection }
    it { should     be_client_error }
    it { should_not be_server_error }
  end

  describe 'with 5xx status' do
    subject { HTTPkit::Response.new(500) }

    its(:status_class) { should be(5) }

    it { should_not be_informational }
    it { should_not be_successful }
    it { should_not be_redirection }
    it { should_not be_client_error }
    it { should     be_server_error }
  end
end
