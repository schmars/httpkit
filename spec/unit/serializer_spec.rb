# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Serializer do
  shared_context :serializer do
    let(:yielded_chunks) { [] }
    let(:writer) { proc { |chunk| yielded_chunks << chunk } }

    subject! { described_class.new(message, writer).serialize }

    it 'serializes' do
      expect(yielded_chunks).to eq(chunks)
    end
  end

  describe 'with simple request' do
    let(:message) do
      HTTPkit::Request.new(:get, '/asd?k=v', { 'Key' => 'value' }, 'hello')
    end

    let(:chunks) do
      ["GET /asd?k=v HTTP/1.1\r\n",
       "Key: value\r\nContent-Length: 5\r\n\r\n",
       'hello']
    end

    include_context :serializer
  end

  describe 'with simple response' do
    let(:message) do
      HTTPkit::Response.new(200, { 'Key' => 'value' }, 'hello')
    end

    let(:chunks) do
      ["HTTP/1.1 200 OK\r\n",
       "Key: value\r\nContent-Length: 5\r\n\r\n",
       'hello']
    end

    include_context :serializer
  end

  describe 'with empty body' do
    let(:message) do
      HTTPkit::Response.new(200, 'Key' => 'value')
    end

    let(:chunks) do
      ["HTTP/1.1 200 OK\r\n",
       "Key: value\r\nContent-Length: 0\r\n\r\n",
       '']
    end

    include_context :serializer
  end

  describe 'with readonly body' do
    let(:message) do
      headers = { 'Key' => 'value', 'Transfer-Encoding' => 'chunked' }
      HTTPkit::Response.new(200, headers, 'hello')
    end

    let(:chunks) do
      ["HTTP/1.1 200 OK\r\n",
       "Key: value\r\nContent-Length: 5\r\n\r\n",
       'hello']
    end

    include_context :serializer
  end

  describe 'with writable body', reactor: true do
    let(:message) do
      headers = { 'Key' => 'value', 'Content-Length' => '123' }
      HTTPkit::Response.new(200, headers, HTTPkit::Body.new)
    end

    # With a smaller body it's more difficult to test the base 16 conversion.
    let(:hello) { 'hello' * 204 }

    let(:chunks) do
      ["HTTP/1.1 200 OK\r\n",
       "Key: value\r\nTransfer-Encoding: chunked\r\n\r\n",
       "3fc\r\n#{hello}\r\n",
       "0\r\n\r\n"]
    end

    before do
      EM.next_tick do
        message.body.closed.progress(hello)
        message.body.closed.progress('')
        message.body.closed.fulfill
      end
    end

    include_context :serializer
  end
end
