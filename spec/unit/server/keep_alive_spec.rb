# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Server::KeepAliveHandler do
  let(:config)     { double('config') }
  let(:server)     { double('server') }
  let(:connection) { double('connection') }

  let(:object)  { described_class.new }
  let(:request) do
    double('request', http_version: http_version,
                      headers: { 'Connection' => header })
  end
  let(:served)   { double('served', value: response) }
  let(:response) { double('response', headers: { 'Connection' => header }) }

  before { object.setup(config, server, connection) }

  describe '#close_connection?' do
    before { object.serve(request, served) }

    let(:subject) { object.send(:close_connection?, request) }

    describe 'with HTTP/1.0 request' do
      let(:http_version) { 1.0 }

      describe 'and no header' do
        let(:header) { nil }

        it { should be(true) }
      end

      describe 'and Connection: close' do
        let(:header) { 'close' }

        it { should be(true) }
      end

      describe 'and Connection: keep-alive' do
        let(:header) { 'keep-alive' }

        it { should be(false) }
      end
    end

    describe 'with HTTP/1.1 request' do
      let(:http_version) { 1.1 }

      describe 'and no header' do
        let(:header) { nil }

        it { should be(false) }
      end

      describe 'and Connection: close' do
        let(:header) { 'close' }

        it { should be(true) }
      end

      describe 'and Connection: keep-alive' do
        let(:header) { 'keep-alive' }

        it { should be(false) }
      end
    end
  end
end
