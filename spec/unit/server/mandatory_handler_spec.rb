# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Server::MandatoryHandler do
  describe '#respond' do
    let(:handler) { described_class.new }
    let(:response) { double('response', headers: headers) }
    let(:headers) { {} }

    subject! do
      handler.respond(nil, response) { |_, res| @response = response }
    end

    it 'sets Server and Date headers' do
      expect(@response.headers['Server']).to eq("httpkit/#{HTTPkit::VERSION}")
      expect(@response.headers['Date']).to eq(Time.now.httpdate)
    end

    describe 'with Server or Date header set to anything' do
      let(:headers) { { 'Server' => nil, 'Date' => nil } }

      it 'does not override' do
        expect(@response.headers['Server']).to be(nil)
        expect(@response.headers['Date']).to be(nil)
      end
    end
  end
end
