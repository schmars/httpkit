# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Server do
  let(:server) { described_class.new(config, connection) }

  let(:config) do
    { address: localhost, port: random_port, handlers: [handler] }
  end
  let(:connection) do
    double('connection', :'on_message=' => nil,
                         closed:    double('closed', then: nil),
                         serialize: nil,
                         close: nil)
  end
  let(:handler) do
    double('handler', setup: nil, respond: nil, finish: nil)
  end

  describe '.start' do
    subject { described_class.start(config) }

    it 'binds an EventMachine-backed server instance' do
      expect(HTTPkit::Connection::EventMachine)
        .to receive(:start_server).with(config, described_class)
      subject
    end
  end

  describe '#initialize' do
    subject! { server }

    its(:config) { should be(config) }

    it 'sets up connection' do
      expect(connection).to have_received(:on_message=)
        .with(server.method(:serve))
    end

    it 'sets up handlers' do
      expect(handler).to have_received(:setup).with(config, server, connection)
      expect(config[:handlers].last)
        .to be_a(HTTPkit::Server::MandatoryHandler)
    end

    describe 'without handlers' do
      let(:config) { {} }

      it 'adds the mandatory handler' do
        expect(config[:handlers].last)
          .to be_a(HTTPkit::Server::MandatoryHandler)
      end
    end
  end

  describe '#serve', reactor: true do
    let(:request)  { open_request }
    let(:response) { open_response }

    before do
      allow(server).to  receive(:respond)
      allow(handler).to receive(:serve) { |_, served|
        @served = served
        @fiber = Fiber.current
      }
    end

    subject! { server.serve(request) }

    it 'sets up correlation' do
      @served.fulfill(response)
      tick
      expect(server).to have_received(:respond).with(request, response)
    end

    it 'notifies the handlers' do
      expect(handler).to    have_received(:serve)
        .with(request, kind_of(HTTPkit::Promise))
      expect(@fiber).not_to be(Fiber.current)
    end
  end

  describe '#respond' do
    let(:headers)  { {} }
    let(:request)  { open_request }
    let(:response) { open_response(200, headers) }

    before { allow(server).to receive(:finish) }

    subject! { server.respond(request, response) }

    it 'writes response to underlying connection' do
      expect(connection).to have_received(:serialize).with(response)
    end

    it 'notifies the handlers' do
      expect(handler).to have_received(:respond).with(request, response)
    end

    it 'wires up #finish' do
      expect(server).to have_received(:finish).with(request)
    end
  end

  describe '#finish' do
    let(:request) { open_request }

    subject! { server.finish(request) }

    it 'notifies the handlers' do
      expect(handler).to have_received(:finish).with(request)
    end
  end
end
