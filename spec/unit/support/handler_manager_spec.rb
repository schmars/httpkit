# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Support::HandlerManager do
  let(:handler) { double('handler', message: nil) }
  let(:handler2) { double('handler2', message: nil) }
  let(:manager) { described_class.new([handler, handler2]) }

  describe '#invoke' do
    let(:args) { [double, double] }
    let(:args2) { [double, double] }

    describe 'with arguments' do
      subject! do
        manager.invoke(:nonexisting)
        manager.invoke(:message, *args)
      end

      it 'sends message to the handlers' do
        expect(handler).to have_received(:message).with(*args)
        expect(handler2).to have_received(:message).with(*args)
      end

      it 'returns the arguments' do
        expect(subject).to eq(args)
      end
    end

    describe 'with yielding handler' do
      let(:non_array) { double }

      before do
        allow(handler).to receive(:message).and_yield(*args2)
        allow(handler2).to receive(:message).and_yield(non_array)
      end

      subject! do
        manager.invoke(:message, *args)
      end

      it 'updates arguments for next handler' do
        expect(handler).to have_received(:message).with(*args)
        expect(handler2).to have_received(:message).with(*args2)
      end

      it 'returns the updated arguments' do
        expect(subject).to eq([non_array])
      end
    end
  end
end
