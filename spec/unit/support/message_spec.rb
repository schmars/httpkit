# encoding: utf-8

require 'spec_helper'

describe HTTPkit::Support::Message do
  let(:message) do
    double('message', headers: { bar: '123' }, body: body)
      .extend(described_class)
  end

  let(:body) do
    double('body', closed: double('closed', pending?: pending,
                                            sync:     double,
                                            then:     nil,
                                            fulfill:  nil,
                                            reject:   nil))
  end

  let(:pending) { false }

  describe '#closed?' do
    subject { message.closed? }

    it { should be(true) }

    describe do
      let(:pending) { true }

      it { should be(false) }
    end
  end

  describe '#closed!' do
    subject { message.closed! }

    it { should be(body.closed.sync) }
  end

  describe '#closed' do
    subject { message.closed { throw :block } }

    it 'forwards' do
      expect(body.closed).to receive(:then) { |&block|
        expect(block).to throw_symbol(:block)
      }
      subject
    end
  end

  describe '#close' do
    subject! { message.close }

    it 'forwards' do
      expect(body.closed).to have_received(:fulfill)
    end
  end

  describe '#reject_closed' do
    subject! { message.reject_closed(StandardError) }

    it 'forwards' do
      expect(body.closed).to have_received(:reject).with(StandardError)
    end
  end

  describe '.build' do
    before do
      allow(HTTPkit::Body).to receive(:new) { body }
    end

    describe 'given a parser holding a request' do
      let(:parser) do
        Struct.new(:http_method, :request_url, :headers, :http_version)
          .new('GET', '/', { 'Key' => 'value' }, [1, 1])
      end
      let(:args) { [:get, '/', { 'Key' => 'value' }, body] }
      let(:request) { double }

      subject { HTTPkit::Support::Message.build(parser) }

      it 'returns a Request object' do
        expect(HTTPkit::Request)
          .to receive(:new).with(*args).and_return(request)
        expect(request).to receive(:http_version=).with(1.1)
        expect(subject).to be(request)
      end
    end

    describe 'given a parser holding a response' do
      let(:parser) do
        Struct.new(:http_method, :status_code, :headers, :http_version)
          .new(nil, 200, { 'Key' => 'value' }, [1, 0])
      end
      let(:args) { [200, { 'Key' => 'value' }, body] }
      let(:response) { double }

      subject { HTTPkit::Support::Message.build(parser) }

      it 'returns a Response object' do
        expect(HTTPkit::Response)
          .to receive(:new).with(*args).and_return(response)
        expect(response).to receive(:http_version=).with(1.0)
        expect(subject).to be(response)
      end
    end
  end
end
